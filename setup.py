# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from setuptools import setup, find_packages
from os.path import join, dirname
 
setup(
    name='unicom-test-app',
    version='0.0.1',
    description='Тестовое задание компании Юником',
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    author='Igor S. Kovalenko',
    author_email='kovalenko@sb-soft.biz',
    packages=find_packages(),
    platforms='any',
    zip_safe=False,
    include_package_data=True,
    dependency_links=[],
    install_requires=[
        'django==1.11.6',
        'django-autocomplete-light==3.2.10',
        'djangorestframework==3.7.0',
        'django-filter==1.0.4',
        'coreapi==2.3.3',
        'django-rest-swagger==2.1.2',
        'six==1.11.0',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Framework :: Django :: 1.11',
    ],
)
