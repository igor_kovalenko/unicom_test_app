# Тестовое задание компании Юником

## Техническое задание

[Смотрим тут](CONDITIONS.md)

## Установка и конфигурация

+ Создаем каталог проекта


```
#!bash
    mkdir unicom_project
    cd unicom_project
```


+ Создаем и ативируем виртуальное окружение (песочницу)


```
#!bash
    virtualenv env
    source env/bin/activate
```


+ Устанавливаем приложение


```
#!bash
    pip install https://bitbucket.org/igor_kovalenko/unicom_test_app/get/master.tar.gz
```


+ Создаем проект (обратите внимание на точку в команде)


```
#!bash
    django-admin startproject unicom_project .
```


+ Добавляем следующие строчки в unicom_project/settings.py


```
#!python
MIDDLEWARE += [
    'django.middleware.locale.LocaleMiddleware',
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
}

INSTALLED_APPS = [
    'dal',
    'dal_select2',
    'rest_framework',
    'rest_framework.authtoken',
    'django_filters',
    'rest_framework_swagger',
    'unicom_test_app',
] + INSTALLED_APPS

LANGUAGE_CODE = 'ru'

```

+ Добавляем строчки в unicom_project/urls.py


```
#!python
from django.conf.urls import include

urlpatterns += [
    url(r'^unicom/', include('unicom_test_app.urls')),    
]

```


+ Настраиваем проект unicom_project


```
#!bash
    python manage.py migrate
    
    python manage.py createsuperuser
``` 
    

+ Запускаем проект


```
#!bash
    python manage.py runserver
```


+ Опционально можно протестировать проект командой


```
#!bash
    python manage.py test unicom_test_app
```


+ Заходим в свой любимый брузер и видим API по адресу http://127.0.0.1:8000/unicom/api/v1/ , а документацию -  http://127.0.0.1:8000/unicom/docs/#/v1



Enjoy! :sparkles:
