# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.db import transaction
from django.contrib.auth.models import User


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__year__ = "2017"
__description__ = "Тестовое задание компании Юником"


class Organization(models.Model):
    """
    Организация
    """
    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')

    #: Официальное наименование
    name = models.CharField(
        max_length=255, db_index=True, verbose_name=_('Name'), help_text=_('Name'))

    #: ОГРН/ОГРНИП
    reg_no = models.CharField(
        verbose_name=_('State registration number'), max_length=128, db_index=True)

    #: КПП
    crr = models.CharField(
        max_length=16, null=True, blank=True, verbose_name=_('Code of reason for registration'))

    is_credit_organization = models.BooleanField(default=False,
                                                 verbose_name=_('Is credit organization'))

    def __unicode__(self):
        return "{0} ({1})".format(self.name, _('credit organization')) \
            if self.is_credit_organization else self.name

    def __str__(self):
        return "{0} ({1})".format(self.name, _('credit organization')) \
            if self.is_credit_organization else self.name


class AppRole(models.Model):
    """
    Роли приложения
    """
    class Meta:
        verbose_name = _('Application role')
        verbose_name_plural = _('Application roles')

    ROLE_TYPE = (
        ('ADMIN', _('Application administrator')),
        ('PARTNER', _('Partner')),
        ('CO', _('Credit organization')),
    )

    #: Учетная запись включенная в роль
    username = models.ForeignKey(User, verbose_name=_('Username'))

    #: Тип роли
    role_type = models.CharField(max_length=8, choices=ROLE_TYPE, verbose_name=_('Role type'))

    #: Организация
    organization = models.ForeignKey(Organization, verbose_name=_('Organization'))

    def __unicode__(self):
        return "{0} - {1}".format(self.username.username, self.organization)

    def __str__(self):
        return "{0} - {1}".format(self.username.username, self.organization)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Если сотрудник организации имеет роль "Сотрудник кредитной организации",
        # то его организация принудительно получает признак организации кредитной
        if self.role_type == 'CO' and not self.organization.is_credit_organization:
            self.organization.is_credit_organization = True
            self.organization.save()
        super(AppRole, self).save(force_insert, force_update, using, update_fields)


class Questionnaire(models.Model):
    """
    Анкета
    """
    class Meta:
        verbose_name = _('Questionnaire')
        verbose_name_plural = _('Questionnaires')

    #: Номер документа
    doc_no = models.CharField(verbose_name=_('Document No'), max_length=128, db_index=True)

    #: Дата/время создания
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Creation date'))

    #: Дата/время последней модификации
    last_modify_date = models.DateTimeField(auto_now=True, verbose_name=_('Last modify date'))

    #: ФИО
    name = models.CharField(max_length=255, db_index=True, verbose_name=_('Name'))

    #: Дата праздника
    birthday = models.DateField(null=True, blank=True, verbose_name=_('Birthday'))

    #: Номер паспорта
    passport_no = models.CharField(verbose_name=_('Passport No'), max_length=12, db_index=True)

    #: Номер телефона
    phone_no = models.CharField(verbose_name=_('Phone No'), max_length=12, db_index=True)

    #: Скоринговый бал
    scoring = models.PositiveIntegerField(null=True, blank=False, verbose_name=_('Scoring'))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if not self.id:  # Устанавливаем дефолтовые значения
            # Устанавливаем номер документа
            s = Sequence.objects.get_or_create(
                name='questionnaire',
                defaults={
                    'name': 'questionnaire',
                    'prefix': 'QST',
                    'separator': '-',
                }
            )[0]
            self.doc_no = s.request_next_number()

        super(Questionnaire, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return "{0} {1} {2}".format(self.doc_no, _('at'), self.creation_date.date())

    def __str__(self):
        return "{0} {1} {2}".format(self.doc_no, _('at'), self.creation_date.date())


class Offer(models.Model):
    """
    Предложение
    """
    class Meta:
        verbose_name = _('Offer')
        verbose_name_plural = _('Offers')

    OFFER_TYPE = (
        ('CONSUMER', _('Consumer credit')),  # Потребительский кредит
        ('MORTGAGE', _('Mortgage')),         # Ипотека
        ('CAR', _('Car credit')),            # Автокредит
        ('SME', _('SME crediting')),         # Кредитование малого и среднего бизнеса (КМСБ)
    )

    #: Номер документа
    doc_no = models.CharField(verbose_name=_('Document No'), max_length=128, db_index=True)

    #: Дата/время создания
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Creation date'))

    #: Дата/время последней модификации
    last_modify_date = models.DateTimeField(auto_now=True, verbose_name=_('Last modify date'))

    #: Дата/время начала ротации
    begin_rotation_date = models.DateTimeField(
        null=True, blank=True, verbose_name=_('Begin rotation date'))

    #: Дата/время окончания ротации
    end_rotation_date = models.DateTimeField(
        null=True, blank=True, verbose_name=_('End rotation date'))

    #: Наименование
    name = models.CharField(max_length=255, db_index=True, verbose_name=_('Name'))

    #: Тип предложения
    offer_type = models.CharField(max_length=8, choices=OFFER_TYPE, verbose_name=_('Offer type'))

    #: Минимальный скоринговый бал
    min_scoring = models.PositiveIntegerField(null=True, blank=False, verbose_name=_('Min scoring'))

    #: Максимальный скоринговый бал
    max_scoring = models.PositiveIntegerField(null=True, blank=False, verbose_name=_('Max scoring'))

    #: Кредитная организация
    credit_organization = models.ForeignKey(
        Organization, null=True, blank=True, verbose_name=_('Credit organization'),
    )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if not self.id:  # Устанавливаем дефолтовые значения
            # Устанавливаем номер документа
            s = Sequence.objects.get_or_create(
                name='offer',
                defaults={
                    'name': 'offer',
                    'prefix': 'OFR',
                    'separator': '-',
                }
            )[0]
            self.doc_no = s.request_next_number()

        super(Offer, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return "{0} {1} {2}".format(self.doc_no, _('at'), self.creation_date.date())


class Request(models.Model):
    """
    Заявка
    """
    class Meta:
        verbose_name = _('Request')
        verbose_name_plural = _('Requests')

    STATUS = (
        ('NEW', _('New')),
        ('SENT-OUT', _('Sent out')),
        ('RECEIVED', _('Received'))
    )

    #: Номер документа
    doc_no = models.CharField(verbose_name=_('Document No'), max_length=128, db_index=True)

    #: Дата/время создания
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Creation date'))

    #: Владелец (автор) заявки
    owner = models.ForeignKey(User, verbose_name=_('Owner'))

    #: Дата/время отправки
    sent_date = models.DateTimeField(null=True, blank=True, verbose_name=_('Sent date'))

    #: Дата/время получения
    received_date = models.DateTimeField(null=True, blank=True, verbose_name=_('Received date'))

    #: Анкета
    questionnaire = models.ForeignKey(
        Questionnaire, on_delete=models.CASCADE, verbose_name=_('Questionnaire'))

    #: Предложение
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE, verbose_name=_('Offer'))

    #: Статус
    status = models.CharField(max_length=8, choices=STATUS, verbose_name=_('Status'))

    def udate_status(self):
        if self.received_date and self.status == 'SENT-OUT':
            self.status = 'RECEIVED'
            return
        if self.sent_date and not self.received_date and self.status == 'NEW':
            self.status = 'SENT-OUT'
            return
        if not self.sent_date and not self.received_date:
            self.status = 'NEW'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if not self.id:  # Устанавливаем дефолтовые значения
            # Устанавливаем номер документа
            s = Sequence.objects.get_or_create(
                name='request',
                defaults={
                    'name': 'request',
                    'prefix': 'RQ',
                    'separator': '-',
                }
            )[0]
            self.doc_no = s.request_next_number()

        # Обновляем статус
        self.udate_status()

        super(Request, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return "{0} {1} {2}".format(self.doc_no, _('at'), self.creation_date.date())

    def __str__(self):
        return "{0} {1} {2}".format(self.doc_no, _('at'), self.creation_date.date())


class Sequence(models.Model):
    """
    Служебный класс необходимый для генерации номера документа
    """
    class Meta:
        verbose_name = _('Sequence')
        verbose_name_plural = _('Sequences')

    name = models.CharField(max_length=255, verbose_name=_('Name'))
    separator = models.CharField(default='-', blank=True, max_length=1, verbose_name=_('Separator'))
    prefix = models.CharField(max_length=8, null=True, blank=True, verbose_name=_('Prefix'))
    number = models.IntegerField(editable=False, default=0, verbose_name=_('Number'))
    suffix = models.CharField(max_length=8, null=True, blank=True, verbose_name=_('Suffix'))

    @transaction.atomic
    def request_next_number(self):
        s = Sequence.objects.select_for_update().get(id=self.id)
        return s.get_next_number()

    def current_number(self):
        if self.prefix and self.suffix:
            return u"{0}{3}{1}{3}{2}".format(self.prefix, self.number, self.suffix, self.separator)
        if self.prefix:
            return u"{0}{2}{1}".format(self.prefix, self.number, self.separator)
        if self.suffix:
            return u"{0}{2}{1}".format(self.number, self.suffix, self.separator)
        return u"{0}".format(self.number)
    current_number.short_description = _('Current number')

    def get_next_number(self, force_insert=False, force_update=False, using=None):
        self.number += 1
        super(Sequence, self).save(force_insert, force_update, using)
        return self.current_number()

    def reset_sequence(self, force_insert=False, force_update=False, using=None):
        self.number = 0
        super(Sequence, self).save(force_insert, force_update, using)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
