# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from rest_framework import permissions

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__year__ = "2017"
__description__ = "Тестовое задание компании Юником"


class IsAdminOrReadOnly(permissions.BasePermission):
    """
    Разрешение на доступ для изменения членам роли "Администраторы" и для просмотра всем остальным
    """

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user.is_anonymous:
            return False

        # Администраторам предоставлено право на все
        return bool(request.user.approle_set.filter(role_type='ADMIN').count())

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Администраторам предоставлено право на все
        return bool(request.user.approle_set.filter(role_type='ADMIN').count())


class IsAdmin(permissions.BasePermission):
    """
    Разрешение на доступ для членов роли "Администраторы"
    """

    def has_permission(self, request, view):
        return not request.user.is_anonymous and bool(
            request.user.approle_set.filter(role_type='ADMIN').count())

    def has_object_permission(self, request, view, obj):
        return not request.user.is_anonymous and bool(
            request.user.approle_set.filter(role_type='ADMIN').count())


class IsAdminOrPartner(IsAdmin):
    """
    Разрешение на доступ для членов роли "Партнер"
    """

    def has_permission(self, request, view):
        # Администратор приложения может делать все
        if super(IsAdminOrPartner, self).has_permission(request, view):
            return True

        # Партнерам предоставлено право создавать объекты и просматривать список объектов
        return not request.user.is_anonymous and bool(
            request.user.approle_set.filter(role_type='PARTNER').count()) and \
               request.method in ('POST', 'GET', 'HEAD', 'OPTION')

    def has_object_permission(self, request, view, obj):
        # Администратор приложения может делать все
        if super(IsAdminOrPartner, self).has_object_permission(request, view, obj):
            return True

        # Партнерам предоставлено право просматривать объекты
        return not request.user.is_anonymous and bool(
            request.user.approle_set.filter(role_type='PARTNER').count()) and \
               request.method in permissions.SAFE_METHODS


class IsAdminOrPartnerOrCreditOrganization(IsAdminOrPartner):
    """
    Разрешение на доступ для членов роли "Кредитная организация"
    """

    def has_permission(self, request, view):
        # Партнеры и администраторы могут делать все то, что доступно кредитным организациям
        if super(IsAdminOrPartnerOrCreditOrganization, self).has_permission(request, view):
            return True

        # Кредитным организациям предоставлено только просматривать список объектов
        return not request.user.is_anonymous and bool(
            request.user.approle_set.filter(role_type='CO').count()) and \
               request.method in permissions.SAFE_METHODS

    def has_object_permission(self, request, view, obj):
        # Партнеры и администраторы могут делать все то, что доступно кредитным организациям
        if super(IsAdminOrPartnerOrCreditOrganization, self).has_object_permission(
                request, view, obj):
            return True

        # Кредитным организациям предоставлено только просматривать объекты
        return not request.user.is_anonymous and bool(
            request.user.approle_set.filter(role_type='CO').count()) and \
               request.method in permissions.SAFE_METHODS
