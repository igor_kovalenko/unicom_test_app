# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from dal import autocomplete
from django import forms
from unicom_test_app.models import Offer, Questionnaire, Request

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__year__ = "2017"
__description__ = "Тестовое задание компании Юником"


class OfferForm(forms.ModelForm):
    doc_no = forms.CharField(required=False, widget=forms.HiddenInput)

    class Meta:
        widgets = {
            'credit_organization': autocomplete.ModelSelect2(
                url='organization-autocomplete',
                attrs={
                    'data-minimum-input-length': 3,
                    'data-language': 'ru',
                    'data-placeholder': 'Начните вводить название организации'
                }
            )
        }
        model = Offer
        fields = ('__all__')


class QuestionnaireForm(forms.ModelForm):
    doc_no = forms.CharField(required=False, widget=forms.HiddenInput)

    class Meta:
        model = Questionnaire
        fields = ('__all__')


class RequestForm(forms.ModelForm):
    doc_no = forms.CharField(required=False, widget=forms.HiddenInput)
    status = forms.CharField(required=False, widget=forms.HiddenInput)

    class Meta:
        widgets = {
            'offer': autocomplete.ModelSelect2(
                url='offer-autocomplete',
                attrs={
                    'data-minimum-input-length': 3,
                    'data-language': 'ru',
                    'data-placeholder': 'Начните вводить ФИО указанное в предложении'
                }
            ),
            'questionnaire': autocomplete.ModelSelect2(
                url='questionnaire-autocomplete',
                attrs={
                    'data-minimum-input-length': 3,
                    'data-language': 'ru',
                    'data-placeholder': 'Начните вводить ФИО указанное в анкете'
                }
            ),
        }
        model = Request
        fields = ('__all__')
