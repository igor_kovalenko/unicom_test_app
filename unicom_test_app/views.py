# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from dal import autocomplete
from unicom_test_app.models import Organization, Questionnaire, Offer, Request
from unicom_test_app.serializers import OrganizationSerializer, QuestionnaireSerializer, \
    OfferSerializer, RequestSerializer
from unicom_test_app.permissions import IsAdminOrReadOnly, IsAdminOrPartner,\
    IsAdminOrPartnerOrCreditOrganization
from rest_framework.decorators import api_view, detail_route
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import viewsets
from rest_framework import status
from django_filters import rest_framework as filters
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__year__ = "2017"
__description__ = "Тестовое задание компании Юником"


class OrganizationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if self.request.user.is_authenticated() and self.request.user.is_staff:

            qs = Organization.objects.order_by('name')

            if self.q:
                qs = qs.filter(name__istartswith=self.q)

            return qs
        return Organization.objects.none()


class QuestionnaireAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if self.request.user.is_authenticated() and self.request.user.is_staff:

            qs = Questionnaire.objects.order_by('creation_date')

            if self.q:
                qs = qs.filter(name__istartswith=self.q)

            return qs
        return Questionnaire.objects.none()


class OfferAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if self.request.user.is_authenticated() and self.request.user.is_staff:

            qs = Offer.objects.order_by('creation_date')

            if self.q:
                qs = qs.filter(name__istartswith=self.q)

            return qs
        return Offer.objects.none()


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'organizations': reverse('organization-list', request=request, format=format),
        'questionnaire': reverse('questionnaire-list', request=request, format=format),
        'offers': reverse('offer-list', request=request, format=format),
        'requests': reverse('request-list', request=request, format=format),
    })


class OrganizationViewSet(viewsets.ModelViewSet):
    """
    Ресурс 'Организация'

    list:
    Получить список организаций

    create:
    Создать новую организацию

    retrieve:
    Просмотреть данные определенной организации

    destroy:
    Удалить определенную организацию

    update:
    Обновить данные определенной организации

    partial_update:
    Обновить некоторые данные определенной организации

    """
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    lookup_field = 'reg_no'

    permission_classes = [IsAdminOrReadOnly, ]

    @method_decorator(cache_page(60))
    def retrieve(self, request, *args, **kwargs):
        return super(OrganizationViewSet, self).retrieve(request, *args, **kwargs)


class QuestionnaireFilter(filters.FilterSet):
    passport_no = filters.CharFilter(
        name='passport_no',
        label=_('Passport No'),
        help_text=_(
            'Specify the passport No in the format "XX XX XXXXXX", e. c. "48 77 248698"')
    )
    phone_no = filters.CharFilter(
        name='phone_no',
        label=_('Phone No'),
        help_text=_('Specify the phone No in the format "X(XXX)XXXXXXX", e. c. "8(914)7179468"')
    )

    order = filters.OrderingFilter(
        fields=('doc_no', 'creation_date', 'last_modify_date', ),
        field_labels={
            'doc_no': _('Document No'),
            'creation_date': _('Creation date'),
            'last_modify_date': _('Last modify date')
        }
    )

    class Meta:
        model = Questionnaire
        fields = ['passport_no', 'phone_no', ]


class QuestionnaireViewSet(viewsets.ModelViewSet):
    """
    Ресурс 'Анкета'

    list:
    Получить список анкет

    create:
    Создать новую анкету

    retrieve:
    Просмотреть данные определенной анкеты

    destroy:
    Удалить определенную анкету

    update:
    Обновить данные определенной анкеты

    partial_update:
    Обновить некоторые данные определенной анкеты

    """
    queryset = Questionnaire.objects.all()
    serializer_class = QuestionnaireSerializer
    lookup_field = 'doc_no'

    filter_class = QuestionnaireFilter

    permission_classes = [IsAdminOrPartnerOrCreditOrganization, ]

    def get_queryset(self):
        # Члены роли "Администратор" видят все анкеты
        if self.request.user.approle_set.filter(role_type='ADMIN').count():
            return super(QuestionnaireViewSet, self).get_queryset()

        # Партнеры увидят только анкеты на которые ссылаются их собственные заявки
        if self.request.user.approle_set.filter(role_type='PARTNER').count():
            return super(QuestionnaireViewSet, self).get_queryset().filter(
                request__owner=self.request.user
            )

        # Сотрудники кредитной организации увидят только анкеты на которые ссылаются запросы,
        # адресованные их кредитной организации
        if self.request.user.approle_set.filter(role_type='CO').count():
            return super(QuestionnaireViewSet, self).get_queryset().filter(
                request__offer__credit_organization=
                self.request.user.approle_set.first().organization
            )

        # Аутентифицированные, но не включенные в какую-нибудь роль не увидят ничего
        return super(QuestionnaireViewSet, self).get_queryset().none()


class OfferFilter(filters.FilterSet):
    offer_type = filters.ChoiceFilter(
        name='offer_type',
        choices=Offer.OFFER_TYPE,
        label=_('Offer type'),
        help_text=_('Specify offer type. The possible values is ') + ", ".join(
            [of[0] for of in Offer.OFFER_TYPE]
        )
    )
    credit_organization_reg_no = filters.CharFilter(
        name='credit_organization__reg_no',
        label=_('Credit organization reg No'),
        help_text=_('Specify credit organization reg No')
    )
    order = filters.OrderingFilter(
        fields=('doc_no', 'creation_date', 'last_modify_date', ),
        field_labels={
            'doc_no': _('Document No'),
            'creation_date': _('Creation date'),
            'last_modify_date': _('Last modify date')
        }
    )

    class Meta:
        model = Offer
        fields = ['offer_type', 'credit_organization_reg_no', ]


class OfferViewSet(viewsets.ModelViewSet):
    """
    Ресурс 'Предложение'

    list:
    Получить список предложений

    create:
    Создать новое предложение

    retrieve:
    Просмотреть данные определенного предложения

    destroy:
    Удалить определенное предложение

    update:
    Обновить данные определенного предложения

    partial_update:
    Обновить некоторые данные определенного предложения

    """
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer
    lookup_field = 'doc_no'

    filter_class = OfferFilter

    permission_classes = [IsAdminOrPartnerOrCreditOrganization, ]

    def get_queryset(self):
        # Члены роли "Администратор" видят все предложения
        if self.request.user.approle_set.filter(role_type='ADMIN').count():
            return super(OfferViewSet, self).get_queryset()

        # Партнеры увилят только предложения на которые ссылаются их собственные заявки
        if self.request.user.approle_set.filter(role_type='PARTNER').count():
            return super(OfferViewSet, self).get_queryset().filter(
                request__owner=self.request.user
            )

        # Сотрудники кредитной организации увидят только предложения адресованные своей организации
        if self.request.user.approle_set.filter(role_type='CO').count():
            return super(OfferViewSet, self).get_queryset().filter(
                credit_organization=
                self.request.user.approle_set.first().organization
            )

        # Аутентифицированные, но не включенные в какую-нибудь роль не увидят ничего
        return super(OfferViewSet, self).get_queryset().none()


class RequestFilter(filters.FilterSet):
    status = filters.ChoiceFilter(
        name='status',
        choices=Request.STATUS,
        label=_('Status'),
        help_text=_('Specify status code')
    )
    questionnaire_doc_no = filters.CharFilter(
        name='questionnaire__doc_no',
        label=_('Questionnaire No'),
        help_text=_('Specify questionnaire document No')
    )
    offer_doc_no = filters.CharFilter(
        name='offer__doc_no', label=_('Offer No'), help_text=_('Specify offer document No'))

    order = filters.OrderingFilter(
        fields=('doc_no', 'creation_date', 'sent_date', ),
        field_labels={
            'doc_no': _('Document No'),
            'creation_date': _('Creation date'),
            'sent_date': _('Sent date')
        }
    )

    class Meta:
        model = Request
        fields = ['status', 'questionnaire_doc_no', 'offer_doc_no']


class RequestViewSet(viewsets.ModelViewSet):
    """
    Ресурс 'Запрос'

    list:
    Получить список запросов

    create:
    Создать новый запрос

    retrieve:
    Просмотреть данные определенного запроса

    destroy:
    Удалить определенный запрос

    update:
    Обновить данные определенного запроса

    sent:
    Отправить запрос в кредитную организацию

    """
    queryset = Request.objects.all()
    serializer_class = RequestSerializer
    lookup_field = 'doc_no'

    filter_class = RequestFilter

    permission_classes = [IsAdminOrPartnerOrCreditOrganization, ]

    http_method_names = [u'get', u'post', u'put', u'delete', u'head', u'options', u'trace']

    def get_queryset(self):
        # Члены роли "Администратор" видят все заявки
        if self.request.user.approle_set.filter(role_type='ADMIN').count():
            return super(RequestViewSet, self).get_queryset()

        # Партнеры увидят только собственные заявки
        if self.request.user.approle_set.filter(role_type='PARTNER').count():
            return super(RequestViewSet, self).get_queryset().filter(
                owner=self.request.user
            )

        # Сотрудники кредитной организации увидят только заявки адресованные своей организации
        if self.request.user.approle_set.filter(role_type='CO').count():
            return super(RequestViewSet, self).get_queryset().filter(
                offer__credit_organization=
                self.request.user.approle_set.first().organization
            )

        # Аутентифицированные, но не включенные в какую-нибудь роль не увидят ничего
        return super(RequestViewSet, self).get_queryset().none()

    @detail_route(methods=['post'], url_path='sent', permission_classes=[IsAdminOrPartner, ])
    def sent(self, request, doc_no=None):
        """
        Отправить запрос в кредитную организацию
        """
        try:
            r = Request.objects.get(doc_no=doc_no)
            r.sent_date = timezone.now()
            r.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        except ObjectDoesNotExist as e:
            return Response(data=e, status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, *args, **kwargs):
        # Если учетная запись входит в роль "Кредитная организация"
        if request.user.approle_set.filter(role_type='CO').count():
            try:  # Реализация логики установки конечного автомата запроса в состояние "Получено"
                r = Request.objects.get(**kwargs)
                r.received_date = timezone.now()
                r.save()
            except ObjectDoesNotExist as e:
                return Response(data=e, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return super(RequestViewSet, self).retrieve(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        # Получаем экземпляр Запроса
        instance = self.get_object()

        # Сохраняем ссылки на Анкету и Предложени
        offer = instance.offer
        questionnaire = instance.questionnaire

        # Удаляем Запрос
        self.perform_destroy(instance)

        # Удаляем связанные с уже удаленным Запросом Предложение и Анкету
        offer.delete()
        questionnaire.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
        super(RequestViewSet, self).perform_create(serializer)
