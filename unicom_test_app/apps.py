# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UnicomTestAppConfig(AppConfig):
    name = 'unicom_test_app'
    verbose_name = _('Test task of the company Unicom')
