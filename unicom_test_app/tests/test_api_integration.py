# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from unicom_test_app.models import Organization


class IntegrationTestCase(APITestCase):
    fixtures = ['unicom_test_app.json']

    url = reverse("request-list")

    def test_integration(self):
        """
        Интеграционный тест
        """

        # Создаем новый запрос от имени партнера
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(
            self.url,
            {
                "questionnaire": {
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 80
                },
                "offer": {
                    "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
                    "offer_type": "CAR",
                    "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
                }
            }

        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(json.loads(response.content.decode('utf-8')).get('status'), 'NEW')

        request_url = json.loads(response.content.decode('utf-8')).get('url')

        # Отправляем запрос кредитной организации
        response = self.client.post(
            request_url + "sent/",
            {}
        )
        self.assertEqual(202, response.status_code)

        response = self.client.get(request_url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(json.loads(response.content.decode('utf-8')).get('status'), 'SENT-OUT')

        # Просматриваем/получаем запрос от имени кредитной организации
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(request_url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(json.loads(response.content.decode('utf-8')).get('status'), 'RECEIVED')
