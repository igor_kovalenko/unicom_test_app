# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from unicom_test_app.models import Questionnaire, Offer, Request, Organization
from unicom_test_app.serializers import OfferSerializer
from rest_framework.test import APIRequestFactory


class OfferAPIViewTestCase(APITestCase):
    fixtures = ['unicom_test_app.json']

    url = reverse("offer-list")

    def test_create_offer_with_admin_authorization(self):
        """
        Тест на добавление элемента в список от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(self.url, {
            "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
        })
        self.assertEqual(201, response.status_code)

    def test_create_questionnaires_with_partner_authorization(self):
        """
        Тест на добавление элемента в список от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(self.url, {
            "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
        })
        self.assertEqual(201, response.status_code)

    def test_create_questionnaires_with_co_authorization(self):
        """
        Тест на добавление элемента в список от имени кредитной организации
        """
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(self.url, {
            "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
        })
        self.assertEqual(403, response.status_code)

    def test_get_list_questionnaires(self):
        """
        Тест для проверки списка
        """
        Offer.objects.create(**{
            "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ"),
        })
        response = self.client.get(self.url)
        self.assertTrue(len(json.loads(response.content.decode('utf-8'))) == Offer.objects.count())


class OfferDetailAPIViewTestCase(APITestCase):
    fixtures = ['unicom_test_app.json']

    def setUp(self):
        self.offer = Offer.objects.create(**{
            "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ"),
        })
        self.url = reverse("offer-detail", kwargs={"doc_no": self.offer.doc_no})

    def test_offer_object_bundle(self):
        """
        Тест для проверки объекта
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        factory = APIRequestFactory()
        request = factory.get(self.url)
        serializer_data = OfferSerializer(
            instance=self.offer,
            context={'request': request}
        ).data
        response_data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(serializer_data, response_data)

    def test_offer_object_retrieve_with_admin_authorization(self):
        """
        Тест на доступ к предложению от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_offer_object_retrieve_with_partner_authorization(self):
        """
        Тест на доступ к предложению от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(self.url)

        # Без заявки сделанной партнером и ссылающейся на предложение,
        # доступ партнера к предложению будут ограничен
        self.assertEqual(404, response.status_code)

        # Создаем завку ссылающуюся на предложение
        qst = Questionnaire.objects.create(**{
            "name": 'Коваленко И. С.',
            "birthday": "1972-08-07",
            "passport_no": "48 77 248698",
            "phone_no": "8(914)7179468",
            "scoring": 80
        })
        Request.objects.create(
            owner=User.objects.get(username="partner"),
            questionnaire=qst,
            offer=self.offer
        )

        # Теперь, когда есть заявка ссылающаяся на анкету, партнер должен получить к анкете доступ
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_offer_object_retrieve_with_co_authorization(self):
        """
        Тест на доступ к предложению от имени кредитной организации
        """

        # Кредитная организация получит доступ к передложению, только если оно адресовано ей
        user = User.objects.get(username="co_okb")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(self.url)
        self.assertEqual(404, response.status_code)

        # Свои предложения кредитная организация смотреть может
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_offer_object_update_with_admin_authorization(self):
        """
        Тест на обновление записи от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # HTTP PUT
        response = self.client.put(self.url, {
            "name": 'Кредит на новый автомобиль Honda Fit 2017 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
        })
        self.assertEqual(200, response.status_code)

        # HTTP PATCH
        response = self.client.patch(self.url, {
            "name": 'Кредит на новый автомобиль Toyota Tundra 2017 года выпуска'})
        self.assertEqual(200, response.status_code)

    def test_offer_object_update_with_partner_authorization(self):
        """
        Тест на обновление записи от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # HTTP PUT
        response = self.client.put(self.url, {
            "name": 'Кредит на новый автомобиль Honda Fit 2017 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
        })
        self.assertEqual(403, response.status_code)

        # HTTP PATCH
        response = self.client.patch(self.url, {
            "name": 'Кредит на новый автомобиль Toyota Tundra 2017 года выпуска'})
        self.assertEqual(403, response.status_code)

    def test_offer_object_update_with_co_authorization(self):
        """
        Тест на обновление записи от имени кредитной организации
        """
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # HTTP PUT
        response = self.client.put(self.url, {
            "name": 'Кредит на новый автомобиль Honda Fit 2017 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
        })
        self.assertEqual(403, response.status_code)

        # HTTP PATCH
        response = self.client.patch(self.url, {
            "name": 'Кредит на новый автомобиль Toyota Tundra 2017 года выпуска'})
        self.assertEqual(403, response.status_code)

    def test_offer_object_delete_with_admin_authorization(self):
        """
        Тест на удаление записи от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.delete(self.url)
        self.assertEqual(204, response.status_code)

    def test_offer_object_delete_with_partner_authorization(self):
        """
        Тест на удаление записи от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.delete(self.url)
        self.assertEqual(403, response.status_code)

    def test_offer_object_delete_with_co_authorization(self):
        """
        Тест на удаление записи от имени кредитной организации
        """
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.delete(self.url)
        self.assertEqual(403, response.status_code)
