# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from unicom_test_app.models import Questionnaire, Offer, Request, Organization
from unicom_test_app.serializers import RequestSerializer
from rest_framework.test import APIRequestFactory
from django.utils import timezone


class RequestAPIViewTestCase(APITestCase):
    fixtures = ['unicom_test_app.json']

    url = reverse("request-list")

    def test_create_request_with_admin_authorization(self):
        """
        Тест на добавление элемента в список от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(
            self.url,
            {
                "questionnaire": {
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 80
                },
                "offer": {
                    "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
                    "offer_type": "CAR",
                    "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
                }
            }
        )
        self.assertEqual(201, response.status_code)

    def test_create_questionnaires_with_partner_authorization(self):
        """
        Тест на добавление элемента в список от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(
            self.url,
            {
                "questionnaire": {
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 80
                },
                "offer": {
                    "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
                    "offer_type": "CAR",
                    "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
                }
            }

        )
        self.assertEqual(201, response.status_code)

    def test_create_questionnaires_with_co_authorization(self):
        """
        Тест на добавление элемента в список от имени кредитной организации
        """
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(
            self.url,
            {
                "questionnaire": {
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 80
                },
                "offer": {
                    "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
                    "offer_type": "CAR",
                    "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
                }
            }

        )
        self.assertEqual(403, response.status_code)

    def test_get_list_questionnaires(self):
        """
        Тест для проверки списка
        """
        qst = Questionnaire.objects.create(**{
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 80
                })
        ofr = Offer.objects.create(**{
            "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ"),
        })
        Request.objects.create(
            owner=User.objects.get(username="admin"),
            questionnaire=qst,
            offer=ofr
        )
        response = self.client.get(self.url)
        self.assertTrue(
            len(json.loads(response.content.decode('utf-8'))) == Request.objects.count())


class RequestDetailAPIViewTestCase(APITestCase):
    fixtures = ['unicom_test_app.json']

    def setUp(self):
        qst = Questionnaire.objects.create(**{
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 80
                })
        ofr = Offer.objects.create(**{
            "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ"),
        })
        self.request = Request.objects.create(
            owner=User.objects.get(username="admin"),
            questionnaire=qst,
            offer=ofr
        )
        self.url = reverse("request-detail", kwargs={"doc_no": self.request.doc_no})

    def test_request_object_bundle(self):
        """
        Тест для проверки объекта
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        factory = APIRequestFactory()
        request = factory.get(self.url)
        serializer_data = RequestSerializer(
            instance=self.request,
            context={'request': request}
        ).data
        response_data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(serializer_data, response_data)

    def test_request_object_retrieve_with_admin_authorization(self):
        """
        Тест на доступ к запросу от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_request_object_retrieve_with_partner_authorization(self):
        """
        Тест на доступ к запросу от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(self.url)

        # Пратнер видит только свои собственные запросы
        self.assertEqual(404, response.status_code)

        # Создаем запрос от имени партнера
        qst = Questionnaire.objects.create(**{
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 80
                })
        ofr = Offer.objects.create(**{
            "name": 'Кредит на новый автомобиль Toyota Camry Exclusive 2016 года выпуска',
            "offer_type": "CAR",
            "credit_organization": Organization.objects.get(name="НБКИ"),
        })
        self.request = Request.objects.create(
            owner=User.objects.get(username="partner"),
            questionnaire=qst,
            offer=ofr
        )
        url = reverse("request-detail", kwargs={"doc_no": self.request.doc_no})

        # К этому запросу у партнера должен быть доступ
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_request_object_retrieve_with_co_authorization(self):
        """
        Тест на доступ к предложению от имени кредитной организации
        """

        # Кредитная организация получит доступ к запросу,
        # только если предложение связанное с запросом адресовано ей
        user = User.objects.get(username="co_okb")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(self.url)
        self.assertEqual(404, response.status_code)

        # "Свои" предложения кредитная организация смотреть может
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # Переводим запрос на уровень "Отправлен"
        self.request.sent_date = timezone.now()
        self.request.save()

        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        # После просмотра кредитной организацией, запрос должен перейти на уровень "Получен"
        self.assertEqual(json.loads(response.content.decode('utf-8')).get('status'), 'RECEIVED')

    def test_request_object_update_with_admin_authorization(self):
        """
        Тест на обновление записи от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # HTTP PUT
        response = self.client.put(
            self.url,
            {
                "questionnaire": {
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 100
                },
                "offer": {
                    "name": 'Кредит на новый автомобиль Toyota Tundra 2017 года выпуска',
                    "offer_type": "CAR",
                    "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
                }
            }
        )
        self.assertEqual(200, response.status_code)
        self.request = Request.objects.get(pk=self.request.pk)
        self.assertEqual(self.request.questionnaire.scoring, 100)
        self.assertEqual(self.request.offer.name,
                         'Кредит на новый автомобиль Toyota Tundra 2017 года выпуска')

        # HTTP PATCH
        response = self.client.patch(
            self.url,
            {
                "offer:": {
                    "name": 'Кредит на новый автомобиль Honda Fit 2017 года выпуска'
                }
            }
        )
        self.assertEqual(405, response.status_code)

    def test_request_object_update_with_partner_authorization(self):
        """
        Тест на обновление записи от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # HTTP PUT
        response = self.client.put(
            self.url,
            {
                "questionnaire": {
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 100
                },
                "offer": {
                    "name": 'Кредит на новый автомобиль Toyota Tundra 2017 года выпуска',
                    "offer_type": "CAR",
                    "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
                }
            }
        )
        self.assertEqual(403, response.status_code)

        # HTTP PATCH
        response = self.client.patch(
            self.url,
            {
                "offer:": {
                    "name": 'Кредит на новый автомобиль Honda Fit 2017 года выпуска'
                }
            }
        )
        self.assertEqual(403, response.status_code)

    def test_request_object_update_with_co_authorization(self):
        """
        Тест на обновление записи от имени кредитной организации
        """
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # HTTP PUT
        response = self.client.put(
            self.url,
            {
                "questionnaire": {
                    "name": 'Коваленко И. С.',
                    "birthday": "1972-08-07",
                    "passport_no": "48 77 248698",
                    "phone_no": "8(914)7179468",
                    "scoring": 100
                },
                "offer": {
                    "name": 'Кредит на новый автомобиль Toyota Tundra 2017 года выпуска',
                    "offer_type": "CAR",
                    "credit_organization": Organization.objects.get(name="НБКИ").reg_no,
                }
            }
        )
        self.assertEqual(403, response.status_code)

        # HTTP PATCH
        response = self.client.patch(
            self.url,
            {
                "offer:": {
                    "name": 'Кредит на новый автомобиль Honda Fit 2017 года выпуска'
                }
            }
        )
        self.assertEqual(403, response.status_code)

    def test_request_object_delete_with_admin_authorization(self):
        """
        Тест на удаление записи от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.delete(self.url)
        self.assertEqual(204, response.status_code)

    def test_request_object_delete_with_partner_authorization(self):
        """
        Тест на удаление записи от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.delete(self.url)
        self.assertEqual(403, response.status_code)

    def test_request_object_delete_with_co_authorization(self):
        """
        Тест на удаление записи от имени кредитной организации
        """
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.delete(self.url)
        self.assertEqual(403, response.status_code)

    def test_request_send_with_partner_authorization(self):
        """
        Тест на отправку запроса кредитной организации
        """

        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(self.url+"sent/")
        self.assertEqual(403, response.status_code)

        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(self.url+"sent/")
        self.assertEqual(202, response.status_code)
        self.assertEqual(Request.objects.get(pk=self.request.pk).status, 'SENT-OUT')

        self.request.sent_date = None
        self.request.received_date = None
        self.request.save()
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(self.url+"sent/")
        self.assertEqual(202, response.status_code)
        self.assertEqual(Request.objects.get(pk=self.request.pk).status, 'SENT-OUT')
