# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from unicom_test_app.models import Organization
from unicom_test_app.serializers import OrganizationSerializer
from rest_framework.test import APIRequestFactory


class OrganizationAPIViewTestCase(APITestCase):
    fixtures = ['unicom_test_app.json']

    url = reverse("organization-list")

    def setUp(self):
        self.token = Token.objects.create(user=User.objects.get(username='admin'))
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_create_organization(self):
        """
        Тест на добавление элемента в список
        """
        response = self.client.post(self.url, {
            "name": 'Некая организация',
            "reg_no": "12745782011",
            "crr": "342345",
            "is_credit_organization": False
        })
        self.assertEqual(201, response.status_code)

    def test_get_list_organization(self):
        """
        Тест проверки возврата списка объектов
        """
        response = self.client.get(self.url)
        self.assertTrue(
            len(json.loads(response.content.decode('utf-8'))) == Organization.objects.count())


class OrganizationDetailAPIViewTestCase(APITestCase):
    fixtures = ['unicom_test_app.json']

    def setUp(self):
        self.organization = Organization.objects.get(reg_no="12345789019")
        self.url = reverse("organization-detail", kwargs={"reg_no": "12345789019"})

    def test_organization_object_bundle(self):
        """
        Тест проверки целостности объекта
        """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        factory = APIRequestFactory()
        request = factory.get(self.url)
        org_serializer_data = OrganizationSerializer(
            instance=self.organization,
            context={'request': request}
        ).data
        response_data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(org_serializer_data, response_data)

    def test_organization_object_update_with_admin_authorization(self):
        """
        Тест на обновление записи от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # HTTP PUT
        response = self.client.put(self.url, {
            "name": 'Некая организация',
            "reg_no": "12345789019",
            "crr": "342345",
            "is_credit_organization": False
        })
        self.assertEqual(200, response.status_code)

        # HTTP PATCH
        response = self.client.patch(self.url, {"name": 'Еще одна организация'})
        self.assertEqual(200, response.status_code)

    def test_organization_object_update_with_partner_authorization(self):
        """
        Тест на обновление записи от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # HTTP PUT
        response = self.client.put(self.url, {
            "name": 'Некая организация',
            "reg_no": "12745782011",
            "crr": "342345",
            "is_credit_organization": False
        })
        self.assertEqual(403, response.status_code)

        # HTTP PATCH
        response = self.client.patch(self.url, {
            "name": 'Некая организация',
        })
        self.assertEqual(403, response.status_code)

    def test_organization_object_update_with_co_authorization(self):
        """
        Тест на обновление записи от имени кредитной организации
        """

        # Тестируем доступ кредитной организации
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # HTTP PUT
        response = self.client.put(self.url, {
            "name": 'Некая организация',
            "reg_no": "12745782011",
            "crr": "342345",
            "is_credit_organization": False
        })
        self.assertEqual(403, response.status_code)

        # HTTP PATCH
        response = self.client.patch(self.url, {
            "name": 'Некая организация',
        })
        self.assertEqual(403, response.status_code)

    def test_organization_object_delete_with_admin_authorization(self):
        """
        Тест на удаление записи от имени администратора приложения
        """
        user = User.objects.get(username="admin")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.delete(self.url)
        self.assertEqual(204, response.status_code)

    def test_organization_object_delete_with_partner_authorization(self):
        """
        Тест на удаление записи от имени партнера
        """
        user = User.objects.get(username="partner")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.delete(self.url)
        self.assertEqual(403, response.status_code)

    def test_organization_object_delete_with_co_authorization(self):
        """
        Тест на удаление записи от имени кредитной организации
        """
        user = User.objects.get(username="co_nbki")
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.delete(self.url)
        self.assertEqual(403, response.status_code)
