# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.conf.urls import url, include
from unicom_test_app.views import OrganizationAutocomplete, QuestionnaireAutocomplete, \
    OfferAutocomplete, api_root, OrganizationViewSet, QuestionnaireViewSet, OfferViewSet, \
    RequestViewSet
from rest_framework.routers import DefaultRouter
# from rest_framework.documentation import include_docs_urls
#
from rest_framework_swagger.views import get_swagger_view
schema_view = get_swagger_view(title='Описание API')


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__year__ = "2017"
__description__ = "Тестовое задание компании Юником"


router = DefaultRouter()
router.register(r'organizations', OrganizationViewSet)
router.register(r'questionnaires', QuestionnaireViewSet)
router.register(r'offers', OfferViewSet)
router.register(r'requests', RequestViewSet)


urlpatterns = [
    url(r'^organization-autocomplete/$', OrganizationAutocomplete.as_view(),
        name='organization-autocomplete',),
    url(r'^questionnaire-autocomplete/$', QuestionnaireAutocomplete.as_view(),
        name='questionnaire-autocomplete',),
    url(r'^offer-autocomplete/$', OfferAutocomplete.as_view(),
        name='offer-autocomplete',),
    url(r'^api/v1/$', api_root),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/v1/', include(router.urls)),
    # url(r'^docs/', include_docs_urls(title='Описание API'))
    url(r'^docs/', schema_view)
]

# urlpatterns += router.urls
