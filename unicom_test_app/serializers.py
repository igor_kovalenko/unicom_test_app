# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from unicom_test_app.models import Organization, Questionnaire, Offer, Request
from django.utils.translation import ugettext_lazy as _

# from django.core.exceptions import ObjectDoesNotExist
# from collections import OrderedDict


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__year__ = "2017"
__description__ = "Тестовое задание компании Юником"


class OrganizationSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.CharField(
        max_length=255, required=True, label=_('Name'), help_text=_('Max name length 255 symbols'))
    reg_no = serializers.CharField(
        max_length=128,
        required=True,
        label=_('State registration number'),
        help_text=_('Specify organization\'s state registration number')
    )
    crr = serializers.CharField(
        max_length=16,
        required=True,
        label=_('Code of reason for registration'),
        help_text=_('Specify code of reason for registration')
    )

    class Meta:
        model = Organization
        fields = ('url', 'name', 'reg_no', 'crr',)
        extra_kwargs = {
            'url': {'lookup_field': 'reg_no'}
        }


class QuestionnaireSerializer(serializers.HyperlinkedModelSerializer):
    doc_no = serializers.ReadOnlyField(
        label=_('Document No'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    creation_date = serializers.DateTimeField(
        read_only=True,
        label=_('Creation date'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    last_modify_date = serializers.DateTimeField(
        read_only=True,
        label=_('Last modify date'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    name = serializers.CharField(
        max_length=255, required=True, label=_('Name'), help_text=_('Max name length 255 symbols'))
    birthday = serializers.DateField(
        required=True,
        label=_('Birthday'),
        help_text=_('Specify birthday in the format YYYY-MM-DD, e. c. 2017-04-01')
    )
    passport_no = serializers.RegexField(
        "\d{2}\s{1}\d{2}\s{1}\d{6}",
        error_messages={
            'invalid':
                _('Passport No must be in format "XX XX XXXXXX", e. c. "48 77 248698"')
        },
        help_text=_(
            'Specify the passport No in the format "XX XX XXXXXX", e. c. "48 77 248698"')
    )
    phone_no = serializers.RegexField(
        "\d{1}\(\d{3}\)\d{7}",
        error_messages={
            'invalid':
                _('Phone No must be in format "X(XXX)XXXXXXX", e. c. "8(914)7179468"')
        },
        help_text=_(
            'Specify the phone No in the format "X(XXX)XXXXXXX", e. c. "8(914)7179468"')
    )
    scoring = serializers.IntegerField(
        min_value=0,
        required=False,
        label=_('Scoring'),
        help_text=_('Specify scoring. Value must be positive.')
    )

    class Meta:
        model = Questionnaire
        fields = ('url', 'doc_no', 'creation_date', 'last_modify_date', 'name', 'birthday',
                  'passport_no', 'phone_no', 'scoring', )
        extra_kwargs = {
            'url': {'lookup_field': 'doc_no'}
        }


class OfferSerializer(serializers.HyperlinkedModelSerializer):
    doc_no = serializers.ReadOnlyField(
        label=_('Document No'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    creation_date = serializers.DateTimeField(
        read_only=True,
        label=_('Creation date'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    last_modify_date = serializers.DateTimeField(
        read_only=True,
        label=_('Last modify date'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    begin_rotation_date = serializers.DateField(
        required=False,
        label=_('Begin rotation date'),
        help_text=_('Specify begin rotation date in the format YYYY-MM-DD, e. c. 2017-04-01')
    )
    end_rotation_date = serializers.DateField(
        required=False,
        label=_('End rotation date'),
        help_text=_('Specify end rotation date in the format YYYY-MM-DD, e. c. 2017-04-01')
    )
    name = serializers.CharField(
        max_length=255, required=True, label=_('Name'), help_text=_('Max name length 255 symbols'))
    offer_type = serializers.ChoiceField(
        choices=Offer.OFFER_TYPE,
        required=True,
        label=_('Offer type'),
        help_text=_('Specify offer type. The possible values is ') + ", ".join(
            [of[0] for of in Offer.OFFER_TYPE]
        )
    )
    min_scoring = serializers.IntegerField(
        min_value=0,
        required=False,
        label=_('Min scoring'),
        help_text=_('Specify min scoring. Value must be positive.')
    )
    max_scoring = serializers.IntegerField(
        min_value=0,
        required=False,
        label=_('Max scoring'),
        help_text=_('Specify max scoring. Value must be positive.')
    )
    credit_organization = serializers.SlugRelatedField(
        required=True,
        queryset=Organization.objects.filter(is_credit_organization=True),
        slug_field='reg_no',
        read_only=False,
        label=_('Credit organization'),
        help_text=_('Specify credit organization\'s state registration number')
    )

    class Meta:
        model = Offer
        fields = ('url', 'doc_no', 'creation_date', 'last_modify_date', 'begin_rotation_date',
                  'end_rotation_date', 'name', 'offer_type', 'min_scoring', 'max_scoring',
                  'credit_organization')

        extra_kwargs = {
            'url': {'lookup_field': 'doc_no'}
        }


class RequestSerializer(serializers.HyperlinkedModelSerializer):
    doc_no = serializers.ReadOnlyField(
        label=_('Document No'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    creation_date = serializers.DateTimeField(
        read_only=True,
        label=_('Creation date'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    owner = serializers.SlugRelatedField(
        read_only=True,
        slug_field='username',
        label=_('Owner'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    sent_date = serializers.DateTimeField(
        read_only=True,
        label=_('Sent date'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    received_date = serializers.DateTimeField(
        read_only=True,
        label=_('Received date'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )
    questionnaire = QuestionnaireSerializer(
        partial=False,
        label=_('Questionnaire'),
        help_text=_('See "questionnaires > create" for more info')
    )
    offer = OfferSerializer(
        partial=False,
        label=_('Offer'),
        help_text=_('See "offer > create" for more info')
    )
    status = serializers.ReadOnlyField(
        label=_('Status'),
        help_text=_('Do not specify the value of this field, application will do it yourself!')
    )

    class Meta:
        model = Request
        fields = ('url', 'doc_no', 'creation_date', 'owner', 'sent_date', 'received_date',
                  'questionnaire', 'offer', 'status', )
        extra_kwargs = {
            'url': {'lookup_field': 'doc_no'}
        }

    def create(self, validated_data):
        """
        Создать Запрос, включая в него вложенные документы
        :param validated_data: данные для построения Запроса
        :return: экземпляр Запроса
        """

        offer_data = validated_data.pop('offer')

        # На самом деле, к этому моменту, в поле credit_organization уже находится экземпляр
        # Organization, и если его там оставить, то валидация будет провалена.
        # Поэтому заменяем его на значение reg_no, соответствующее lookup_field сериализатора
        # OrganizationSerializer
        offer_data['credit_organization'] = offer_data['credit_organization'].reg_no

        sr = OfferSerializer(data=dict(offer_data))
        if not sr.is_valid():
            raise ValidationError(sr.error_messages)
        offer = sr.save()

        sr = QuestionnaireSerializer(data=validated_data.pop('questionnaire'))
        if not sr.is_valid():
            raise ValidationError(sr.error_messages)
        questionnaire = sr.save()

        return Request.objects.create(offer=offer, questionnaire=questionnaire, **validated_data)

    def update(self, instance, validated_data):
        offer = instance.offer
        questionnaire = instance.questionnaire

        if 'offer' in validated_data:
            offer_data = validated_data.pop('offer')

            # На самом деле, к этому моменту, в поле credit_organization уже может находится
            # экземпляр Organization, и если его там оставить, то валидация будет провалена.
            # Поэтому заменяем его на значение reg_no, соответствующее lookup_field сериализатора
            # OrganizationSerializer
            offer_data['credit_organization'] = offer_data['credit_organization'].reg_no \
                if type(offer_data['credit_organization']) == Organization else \
                offer_data['credit_organization']

            sr = OfferSerializer(instance=instance.offer, data=dict(offer_data))

            if not sr.is_valid():
                raise ValidationError(sr.error_messages)
            offer = sr.save()

        if 'questionnaire' in validated_data:
            sr = QuestionnaireSerializer(instance=instance.questionnaire,
                                         data=validated_data.pop('questionnaire'))
            if not sr.is_valid():
                raise ValidationError(sr.error_messages)
            questionnaire = sr.save()

        Request.objects.update(
            offer=offer, questionnaire=questionnaire, id=instance.pk, **validated_data)

        return Request.objects.get(pk=instance.pk)
