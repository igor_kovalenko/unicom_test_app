# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from unicom_test_app.models import Organization, AppRole, Questionnaire, Offer, Request, Sequence
from unicom_test_app.forms import OfferForm, QuestionnaireForm, RequestForm
import sys

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__year__ = "2017"
__description__ = "Тестовое задание компании Юником"


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name', 'reg_no', 'crr', 'is_credit_organization')
    list_display_links = ('name',)

admin.site.register(Organization, OrganizationAdmin)


class AppRoleAdmin(admin.ModelAdmin):
    list_display = ('username', 'role_type', 'organization', )
    list_display_links = ('username',)
    search_fields = ('organization__name', )
    list_filter = ('role_type', )

admin.site.register(AppRole, AppRoleAdmin)


class QuestionnaireAdmin(admin.ModelAdmin):
    list_display = ('doc_no', 'creation_date', 'last_modify_date', 'name', 'phone_no', 'scoring', )
    list_display_links = ('doc_no', 'name', )
    search_fields = ('doc_no', 'name',)
    date_hierarchy = 'creation_date'
    form = QuestionnaireForm

admin.site.register(Questionnaire, QuestionnaireAdmin)


class OfferAdmin(admin.ModelAdmin):
    list_display = (
        'doc_no', 'creation_date', 'last_modify_date', 'name', 'offer_type',
        'credit_organization',
    )
    list_display_links = ('doc_no', 'name', )
    search_fields = ('doc_no', 'name',)
    list_filter = ('offer_type', 'credit_organization', )
    date_hierarchy = 'creation_date'
    form = OfferForm

admin.site.register(Offer, OfferAdmin)


class RequestAdmin(admin.ModelAdmin):
    list_display = ('doc_no', 'creation_date', 'owner', 'sent_date', 'questionnaire', 'offer',
                    'owner_organization', 'status', )
    readonly_fields = ('owner_organization',)
    list_display_links = ('doc_no', )
    search_fields = ('doc_no', 'offer__name', 'offer__doc_no', 'questionnaire__name',
                     'questionnaire__doc_no', )
    list_filter = ('owner', 'status', )
    date_hierarchy = 'creation_date'
    form = RequestForm

    def owner_organization(self, obj):
        if sys.version_info.major < 3:
            return unicode(obj.owner.approle_set.first().organization)
        return str(obj.owner.approle_set.first().organization)

    owner_organization.short_description = _('Owner\'s organization')

admin.site.register(Request, RequestAdmin)


def request_next_number(modeladmin, request, queryset):
    for document in queryset.iterator():
        document.request_next_number()
request_next_number.short_description = _('Request next number')


class SequenceAdmin(admin.ModelAdmin):
    list_display = ('name', 'current_number', )
    readonly_fields = ('number', )

    actions = [request_next_number, ]

admin.site.register(Sequence, SequenceAdmin)

